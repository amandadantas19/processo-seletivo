var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
        funcionario: {},
        listaSetores: [],
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		],
		editando: false,
		setor:{}
    },
    created: function(){
        let vm =  this;
        vm.buscaFuncionarios();
		vm.buscaSetores();
		if(Array.isArray(listaSetores) && listaSetores.lenght){
			vm.setores = false;
		}
		else{
			vm.setores = true;
		}
    },
    methods:{
        buscaFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios/listar")
			.then(response => {vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		buscaSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setores/listar")
			.then(response => {
				vm.listaSetores = response.data;
			})
			.cath(function (error){
				alert("Error interno","Não foi possível listar setores")
			})
			.finally(function(){	
			});
		},
		cadastrarSetor: function(){
			const vm = this;
			axios.post("/funcionarios/rs/setores/adicionar",this.setor)
			.then(function(response){
				console.log(response);
			})
			.catch(function(error){
				console.log(error);
				alert("Erro ao cadastrar setor");
			})
			.finally(function(){
				vm.buscaSetores();
				this.setores = false;
			})
		},
		cadastrar: function(){
			const vm = this;
			axios.post("/funcionarios/rs/funcionarios/adicionar", this.funcionario)
			.then(function(response){
				console.log(response);
			})
			.catch(function(error){
				console.log(error);
				alert("Erro ao cadastrar");
			})
			.finally(function(){
				vm.buscaFuncionarios();
			});
		},
		editar: function(id){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios/find/" + id)
			.then(function(response){
				vm.funcionario = response.data;
				vm.editando = true;
			})
			.catch(function(error){
				console.log(error);
				alert("Errro ao editar");
			})
		},
		salvar: function(){
			const vm = this;
			axios.put("/funcionarios/rs/funcionarios/atualizar/"+ this.funcionario.id,this.funcionario)
			.then(function(response){
				alert("Funcionario editado com sucesso");
			})
			.catch(function(error){
				alert("Erro ao editar");
			})
			.finally(function(){
				vm.buscaFuncionarios();
				vm.editando = false;
				vm.funcionario = {};
			})
		},
		deletar: function(id) {
			const vm = this;
			axios.delete("/funcionarios/rs/funcionarios/deletar/" + id)
				.then(function(response) {
					console.log(response);
					alert("funcionario removido");
				})
				.catch(function(error){
					console.log(error);
					alert("Error ao remover");
				})
				.finally(function(){
					vm.buscaFuncionarios();
				});
		}
    }
});